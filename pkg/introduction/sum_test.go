package introduction_test

import (
	. "testing-with-ginkgo/pkg/introduction"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Summarize", func() {

	It("Should return 3", func() {
		result := Sum(1, 2)
		Expect(result).To(Equal(3))
	})

	It("Should return -3", func() {
		result := Sum(-1, -2)

		Expect(result).To(Equal(-3))
	})
})
