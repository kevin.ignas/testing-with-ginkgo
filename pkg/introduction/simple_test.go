package introduction_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Simple test", func() {
	It("Should return 1", func() {
		Expect(1).To(Equal(2))
	})

	// contoh failed dan muncul pesan dari By
	It("Should return 1", Pending, func() {
		By("comparing 1 with 2")
		By("second notes")
		Expect(1).To(Equal(2))
	})
})
