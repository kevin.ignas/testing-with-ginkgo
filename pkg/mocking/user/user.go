package user

import "testing-with-ginkgo/pkg/mocking/doer"

type User struct {
	Doer         doer.Doer
	InternalDoer InternalDoer
}

func (u *User) Use() string {
	return u.Doer.DoSomething(123, "Hello GoMock")
}

func (u *User) UseInternal() string {
	return u.InternalDoer.DoSomethingNew(123, "Hello GoMock")
}

func (u *User) UseKeduanya() string {
	doerResult := u.Doer.DoSomething(123, "Hello GoMock")

	if doerResult == "berhasil" {
		return "sudah berhasil"
	}

	return u.InternalDoer.DoSomethingNew(123, "Hello GoMock")
}
