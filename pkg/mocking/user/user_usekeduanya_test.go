package user_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/mock/gomock"

	"testing-with-ginkgo/pkg/mocking/user"

	mocks "testing-with-ginkgo/pkg/mocking/mocks"
)

var _ = Describe("User Use", func() {
	var (
		mockCtrl         *gomock.Controller
		mockDoer         *mocks.MockDoer
		mockInternalDoer *mocks.MockInternalDoer
		testUser         *user.User
	)

	/*
		Sebelum test perlu dipersiapkan mock dan class yang akan ditest.
		di sini seperti yang bisa dilihat, menggunakan dependency injection
		untuk memasukkan Doer yang akan dimock nantinya.
	*/
	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockDoer = mocks.NewMockDoer(mockCtrl)
		mockInternalDoer = mocks.NewMockInternalDoer(mockCtrl)
		testUser = &user.User{Doer: mockDoer, InternalDoer: mockInternalDoer}
	})

	It("Mock doer tapi tidak trigger internal", func() {
		mockDoer.EXPECT().DoSomething(123, "Hello GoMock").Return("berhasil").Times(1)

		result := testUser.UseKeduanya()

		Expect(result).To(Equal("sudah berhasil"))
	})

	It("Mock doer dan trigger internal", func() {
		mockDoer.EXPECT().DoSomething(123, "Hello GoMock").Return("yeay").Times(1)
		mockInternalDoer.EXPECT().DoSomethingNew(123, "Hello GoMock").Return("yeay").Times(1)

		result := testUser.UseKeduanya()

		Expect(result).To(Equal("yeay"))
	})
})
