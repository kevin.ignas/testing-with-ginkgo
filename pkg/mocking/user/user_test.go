package user_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/mock/gomock"

	"testing-with-ginkgo/pkg/mocking/user"

	mocks "testing-with-ginkgo/pkg/mocking/mocks"
)

var _ = Describe("User Use", func() {
	var (
		mockCtrl *gomock.Controller
		mockDoer *mocks.MockDoer
		testUser *user.User
	)

	/*
		Sebelum test perlu dipersiapkan mock dan class yang akan ditest.
		di sini seperti yang bisa dilihat, menggunakan dependency injection
		untuk memasukkan Doer yang akan dimock nantinya.
	*/
	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockDoer = mocks.NewMockDoer(mockCtrl)
		testUser = &user.User{Doer: mockDoer}
	})

	/*
		Doer.DoSomething() di `EXPECT` menerima parameter 123 dan "Hello GoMock" dan
		akan Return value "test" serta dieksekusi 1 kali (Times). Fungsi User.Use()
		hanya return value dari fungsi Doer.DoSomething() sehingga kita bisa simpan
		value tersebut dan gunakan Gomega untuk assert value.
	*/
	It("Mock doer", func() {
		mockDoer.EXPECT().DoSomething(123, "Hello GoMock").Return("test").Times(1)

		result := testUser.Use()

		Expect(result).To(Equal("test"))
	})
})
