package user_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/mock/gomock"

	"testing-with-ginkgo/pkg/mocking/user"

	mocks "testing-with-ginkgo/pkg/mocking/mocks"
)

var _ = Describe("User Use Internal", func() {
	var (
		mockCtrl         *gomock.Controller
		mockInternalDoer *mocks.MockInternalDoer
		testUser         *user.User
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockInternalDoer = mocks.NewMockInternalDoer(mockCtrl)
		testUser = &user.User{InternalDoer: mockInternalDoer}
	})

	It("Doer dari package yang sama", func() {
		mockInternalDoer.EXPECT().DoSomethingNew(123, "Hello GoMock").Return("test internal").Times(1)

		result := testUser.UseInternal()

		Expect(result).To(Equal("test internal"))
	})
})
