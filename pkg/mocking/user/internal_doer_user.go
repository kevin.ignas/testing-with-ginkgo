package user

type InternalDoer interface {
	DoSomethingNew(int, string) string
}
