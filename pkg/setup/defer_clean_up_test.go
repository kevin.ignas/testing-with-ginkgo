package setup_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("DeferCleanup", func() {
	var i = 0

	/*
		reset setelah selesai test suite juga bisa
		diachieve dengan DeferCleanup
	*/
	BeforeEach(func() {

		DeferCleanup(func() {
			i = 0
		})
	})

	It("Should equal 2", func() {
		i = i + 2
		Expect(i).To(Equal(2))
	})

	/*
		i akan bernilai 4 jika i tidak direset
		kembali menjadi 0 pada DeferCleanup, hal
		ini berarti antar test tidak independent
	*/
	It("Should equal 2", func() {
		i = i + 2
		Expect(i).To(Equal(2))
	})
})
