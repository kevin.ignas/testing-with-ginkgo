package setup_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("AfterEach", func() {
	var i = 0

	/*
		i didefine bernilai 0 dan pada setiap test
		nilai i ditambah 2. Dengan AfterEach maka i
		akan selalu direset menjadi 0 setelah test
		suite selesai.
	*/
	AfterEach(func() {
		/*
			j didefine pada pkg/setup/before_and_after_suite_test.go,
			hal ini membuktikan value yang diset pada BeforeSuite bisa
			diakses di spec yang lain.
		*/
		fmt.Println(j)

		i = 0
	})

	It("Should equal 2", func() {
		i = i + 2
		Expect(i).To(Equal(2))
	})

	/*
		i akan bernilai 4 jika i tidak direset
		kembali menjadi 0 pada AfterEach, hal
		ini berarti antar test tidak independent
	*/
	It("Should equal 2", func() {
		i = i + 2
		Expect(i).To(Equal(2))
	})
})
