package setup_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var i, j int

var _ = BeforeSuite(func() {
	fmt.Println("Ini Before Suite")
	j = 299

	i = 100
})

var _ = Describe("BeforeSuite", func() {

	It("Should be 100", func() {
		Expect(i).To(Equal(100))
	})

	It("Should be 102", func() {
		i += 2
		Expect(i).To(Equal(102))
	})

	/*
		BeforeSuite hanya dijalankan satu kali sehingga
		value i adalah 102. Jika test dijalankan secara
		tidak paralel maka akan berhasil namun jika dijalankan
		secara paralel akan gagal karena test tidak independent.
		Test yang tidak independet harus dihindari.
	*/
	It("Should be 104", func() {
		// jika dijalankan secara paralel, expected value menjadi 102
		i += 2
		Expect(i).To(Equal(104))
	})
})

var _ = AfterSuite(func() {
	fmt.Println("Ini After Suite")
})
