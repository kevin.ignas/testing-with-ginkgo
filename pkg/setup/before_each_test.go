package setup_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("BeforeEach", func() {
	var i = 0

	/*
		i didefine bernilai 0 dan pada beforeEach
		nilai i diset menjadi 2
	*/
	BeforeEach(func() {
		i = 2
	})

	It("Should equal 3", func() {
		i += 1
		Expect(i).To(Equal(3))
	})

	It("Should equal 2", func() {
		Expect(i).To(Equal(2))
	})

})
