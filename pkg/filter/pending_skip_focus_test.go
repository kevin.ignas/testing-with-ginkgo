package filter_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Filter", func() {
	It("Ini Pending misal sedang dalam development", Pending, func() {
		Expect(1).To(Equal(3))
	})

	It("Ini Skip, bisa diset secara programmatically", func() {
		Skip("Dihindari")
		Expect(1).To(Equal(3))
	})

	It("Ini Focus", Focus, func() {
		Expect(1).To(Equal(1))
	})

	It("Ini gak Focus, karena ada yang pakai fokus jadi gak dikerjakan", func() {
		Expect(1).To(Equal(1))
	})

})
