package introduction_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestParallelization(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Parallelization Suite")
}
