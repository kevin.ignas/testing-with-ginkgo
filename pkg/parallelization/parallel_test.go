package introduction_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

// jalankan dengan perintah: ginkgo -r -p -v pkg/decorator

var _ = Describe("Parallel", func() {

	BeforeEach(func() {
		fmt.Printf("Ini proses paralel ke-%d \n", GinkgoParallelProcess())
	})

	It("[Random#1] Should return 500", func() {
		Expect(1).To(Equal(1))
	})

	It("[Random#2] Should return 503", func() {
		Expect(100).To(Equal(100))
	})

	It("[Random#3] Should return 201", func() {
		Expect(200).To(Equal(200))
	})

	It("[Random#4] Should return 201", func() {
		Expect(200).To(Equal(200))
	})

	It("[Random#5] Should return 201", func() {
		Expect(200).To(Equal(200))
	})

	It("[Random#6] Should return 201", func() {
		Expect(200).To(Equal(200))
	})

})
