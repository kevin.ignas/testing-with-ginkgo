package subject

import "errors"

func SumPositiveOnly(a, b int) (int, error) {
	if a < 0 || b < 0 {
		return 0, errors.New("only accept positive value")
	}

	return a + b, nil
}

func IsPositive(a int) bool {
	return a >= 0
}
