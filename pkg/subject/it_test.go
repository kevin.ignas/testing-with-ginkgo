package subject_test

import (
	. "testing-with-ginkgo/pkg/subject"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Subject", func() {
	It("Expect To Equal", func() {
		Expect(1).To(Equal(1))
	})

	It("Expect To Not Equal", func() {
		Expect(1).ToNot(Equal(2))
	})

	It("Expect To Be Error", func() {
		_, err := SumPositiveOnly(10, -1)
		Expect(err).To(MatchError("only accept positive value"))
	})

	It("Expect Error To Be Nil", func() {
		result, err := SumPositiveOnly(10, 1)
		Expect(err).To(BeNil())
		Expect(result).To(Equal(11))
	})

	It("Expect Value Is Positive", func() {
		result := IsPositive(10)
		Expect(result).To(BeTrue())
	})

	It("Expect Value Is Negative", func() {
		result := IsPositive(-3)
		Expect(result).To(BeFalse())
	})
})
