package introduction_test

import (
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

// jalankan dengan perintah: ginkgo -r -p -v pkg/decorator

var _ = Describe("Serial", Serial, func() {
	BeforeEach(func() {
		time.Sleep(1 * time.Second)
	})

	It("[SERIAL#1] Should return 1", func() {
		Expect(1).To(Equal(1))
	})

	It("[SERIAL#2] Should return 100", func() {
		Expect(100).To(Equal(100))
	})

	It("[SERIAL#3] Should return 200", func() {
		Expect(200).To(Equal(200))
	})

})
