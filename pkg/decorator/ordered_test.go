package introduction_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

// jalankan dengan perintah: ginkgo -r -p -v pkg/decorator

var _ = Describe("Ordered", func() {
	Describe("Must in order", Ordered, func() {
		BeforeAll(func() {
			fmt.Println("Before All")
		})

		It("[ORDERED#1] Should return 301", func() {
			fmt.Println("Log bantuan melihat alur BeforeAll ORDERED#1")
			Expect(1).To(Equal(1))
		})

		It("[ORDERED#2] Should return 400", func() {
			Expect(100).To(Equal(100))
		})

		It("[ORDERED#3] Should return 404", func() {
			Expect(200).To(Equal(200))
		})

		AfterAll(func() {
			fmt.Println("After All")
		})
	})

	It("[Random#1] Should return 500", func() {
		Expect(1).To(Equal(1))
	})

	It("[Random#2] Should return 503", func() {
		Expect(100).To(Equal(100))
	})

	It("[Random#3] Should return 201", func() {
		Expect(200).To(Equal(200))
	})

})
