package container_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	. "testing-with-ginkgo/pkg/container"
)

var _ = Describe("Context", func() {
	var i float64 = 0

	Context("i = 3", func() {
		It("Should equal 3", func() {
			i = 3
			var expectedRes float64 = 3

			result := Division(9, i)
			Expect(result).To(Equal(expectedRes))
		})
	})

	Context("i = 2", func() {

		It("Should equal 2", func() {
			i = 2

			var expectedRes float64 = 2

			result := Division(4, i)

			Expect(result).To(Equal(expectedRes))
		})

		When("Added by 2", func() {
			It("Should equal 4", func() {
				i = 2
				var expectedRes float64 = 4

				result := Division(4, i)

				result += 2
				Expect(result).To(Equal(expectedRes))
			})
		})
	})
})
