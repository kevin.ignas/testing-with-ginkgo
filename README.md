# About
Proyek ini hanya untuk memudahkan memahami Ginkgo, Gomega, dan Gomock.

# How to test in local
1. clone proyek ini ke local
2. install dependency dengan menjalankan perintah `go get all`
3. jalankan test dengan perintah `ginkgo -r`. Jika perintah `ginkgo` belum terpasang pada komputer maka perlu melakukan setup variabl `$PATH` terlebih dahulu.
4. setelah berhasil maka kamu sudah siap mengembangkan skenario pengujian.
5. untuk memudahkan dalam memahami dan mencoba, maka dapat dengan mengubah skenario-skenario yang sudah ada dan menjalankan test.
